/*sum of section*/

#include "stdafx.h"
#include <iostream>
#include <array>
#include <iomanip>


using namespace std;

int main() {
	int first, second;
	int counter = 0;

	cout << "Podaj pierwsza liczbe: " << endl;
	cin >> first;

	cout << "Podaj druga liczbe: " << endl;
	cin >> second;

	for (int i = first; i <= second; i++) {
		counter += i;

		if (i == second)
			cout << counter << endl;
	}

	system("PAUSE");
	return 0;
}