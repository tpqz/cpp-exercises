/*stars and dots*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <array>

using std::cout;
using std::cin;
using std::endl;

int main() {

	int rows;
	char star[] = "*";
	char dot[] = ".";


	cout << "Podaj liczbe wierszy: ";
	cin >> rows;

	for (int i = 0; i <= rows; i++) {
		cout << endl;
		for (int a = rows; a > i; a--) {
			cout << dot;
		}
		for (int a = 0; a <= i; a++) {
			cout << star;
		}
	}


	cout << endl;

	system("PAUSE");
	return 0;
}