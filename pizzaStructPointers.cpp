/*pizza struct with pointers*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <cstdlib>


using namespace std;

int main() {

	struct pizza {
		string company;
		int diameter;
		int weight;
	};

	pizza * pzz = new pizza;


	cout << "Podaj srednice: ";
	cin >> pzz->diameter;

	cout << "Podaj firme: ";
	cin >> pzz->company;

	cout << "Podaj wage: ";
	cin >> pzz->weight;
	cout << endl;

	cout << "Firma: " << pzz->company << endl;
	cout << "Srednica: " << pzz->diameter << endl;
	cout << "Waga: " << pzz->weight << endl;

	system("PAUSE");
	return 0;
}