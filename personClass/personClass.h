#ifndef PERSONCLASS_H_
#define PERSONCLASS_H_

#include <iostream>
#include <string>
#include <assert.h>

class Person {

private:
	std::string lname;
	std::string fname;

public:
	Person ();
	Person (const std::string ln, const std::string fn);
	Person (const std::string ln);
	void show() const;
	void formalShow() const;
};

#endif



