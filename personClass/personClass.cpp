/*
 * personClass.cpp
 *
 *  Created on: Aug 25, 2016
 *      Author: tqpz
 */
#include "personClass.h"


Person::Person(){
	lname = "Jan";
	fname = "Kowalski";
};


Person::Person(const std::string ln, const std::string fn){
	lname = ln;
	fname = fn;
};

Person::Person(const std::string ln){
	lname = ln;
};


void Person::show() const{
	std::cout << "Imię: " << Person::lname << std::endl;
	std::cout << "Nazwisko: " << Person::fname << std::endl;
	std::cout << "^^^^^^^" << std::endl;
};

void Person::formalShow() const{
	if (Person::lname.empty() && Person::fname.empty()){
		std::cout << "Nie podano imienia i nazwiska\n";

	}else if(Person::fname.empty()){
		std::cout << "Nie podano nazwiska\n";
		std::cout << "Imię: " << Person::lname << std::endl;

	}else if(!Person::lname.empty() && !Person::fname.empty()){
		std::cout << "Imię: " << Person::lname << std::endl;
		std::cout << "Nazwisko: " << Person::fname << std::endl;
	}

	std::cout << std::endl;
};
