/*
 * pClassMain.cpp
 *
 *  Created on: Aug 25, 2016
 *      Author: tqpz
 */
#include <iostream>
#include "personClass.h"

int main(){
	Person one;
	Person Two("Staszek");
	Person Three("Jacek","Placek");

	Three.show();
	Three.formalShow();
	std::cout << "--------------" << std::endl;
	Two.show();
	Two.formalShow();
	std::cout << "--------------" << std::endl;

	one.show();
	one.formalShow();
return 0;
}
