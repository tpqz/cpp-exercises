/*sum of mothly sales*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::cin;
using std::endl;

int main() {

	std::vector<std::string> months = { "Styczen", "Luty", "Marzec", "Kwiecien", "Maj", "Czerwiec",
		"Lipiec", "Sierpien", "Wrzesien", "Pazdziernik", "Listopad", "Grudzien" };
	int sell[12];
	int result = 0;

	for (int i = 0; i <= 11; i++) {
		cout << "Podaj liczbe sprzedazy w " << months[i] << ": ";
		cin >> sell[i];

		result += sell[i];

		if (i == 11) {
			cout << endl;
			cout << "Suma: " << result << endl;
		}
	}

	system("PAUSE");
	return 0;
}