/*tax calculator*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

int main() {

	int payers;

	struct sponsor {
		string name;
		double amount;
	};

	cout << "Podaj liczbe wplacajacych: ";
	cin >> payers;

	sponsor * list = new sponsor[payers];

	for (int i = 0; i < payers; i++) {
		cout << "Podaj nazwisko sponsora nr " << i + 1 << ": ";
		cin >> list[i].name;

		cout << "Podaj kwote jaka wplacil sponsor nr " << i + 1 << ": ";
		cin >> list[i].amount;
	}

	cout << "Nasi Wspaniali Sponsorzy:\n";
	for (int i = 0; i < payers; i++) {
		if (list[i].amount > 10000) {
			cout << list[i].name << endl;
		}
	}

	cout << "Nasi Sponsorzy:\n";
	for (int i = 0; i < payers; i++) {
		if (list[i].amount < 10000) {
			cout << list[i].name << endl;
		}
	}

	system("PAUSE");
	return 0;
}