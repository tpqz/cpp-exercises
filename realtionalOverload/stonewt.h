/*
 * stonewt.h
 *
 *  Created on: Aug 29, 2016
 *      Author: tqpz
 */

#ifndef STONEWT_H_
#define STONEWT_H_
#include <iostream>

using std::ostream;

class Stonewt
{
	private:
		enum {Lbs_per_stn = 14};		// pounds per stone
		int stone;						// whole stones
		double pds_left;				// franctional pounds
		double pounds;					// entire weight in pounds

	public:
		Stonewt(double lbs);			// constructor for double pounds
		Stonewt(int stn, double lbs);	// constructor for stone, lbs
		Stonewt();						// default constructor
		~Stonewt();
		void show_lbs() const;			// show weight in pounds format
		void show_stn() const;	// show weight in stone format
		void set_stn();
		bool operator>(const Stonewt & st) const;
		bool operator<(const Stonewt & st) const;
		bool operator<=(const Stonewt & st) const;
		bool operator>=(const Stonewt & st) const;
		bool operator==(const Stonewt & st) const;
		bool operator!=(const Stonewt & st) const;
		friend ostream & operator<<(ostream & os, const Stonewt & st);
		// conversion functions
		operator int() const;
		operator double() const;

};

#endif
