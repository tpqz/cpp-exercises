/*
 * main.cpp
 *
 *  Created on: Aug 30, 2016
 *      Author: tqpz
 */

#include "stonewt.h"
using std::cout;
using std::cin;

int main(){

	Stonewt array[6];
	Stonewt perfect(11);
	Stonewt max;
	Stonewt min;
	int stns;

	for(int i = 0; i <= 2; i++){
		cout << "Enter weight in pounds: ";
		cin >> stns;
		array[i] = (stns);

		if (array[i] <= min || i == 0){
			min = array[i];
		}

		if (array[i] >= max || i == 0){
			max = array[i];
		}

		if (array[i] == perfect){
			cout << "Index nr " << i << " is perfect\n";
		}

	}

	cout << "Highest value: " << max;
	cout << "Lowest value: " << min;

	for(int i = 0; i <= 2; i++){
		array[i].show_stn();
	}
}
