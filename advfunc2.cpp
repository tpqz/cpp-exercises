/*
 * advfunc2.cpp
 *
 *  Created on: Aug 19, 2016
 *      Author: tqpz
 */

#include <iostream>

using namespace std;

struct batonik {
	char *nazwa;
	double waga;
	int kalorie;
};

void set(batonik & b, char* c="M&M", double d=12.3, int i=512);

int main(){

batonik mars{
	"mars",
	2.13,
	412
};

set(mars);

return 0;
}

void set(batonik & b, char* c, double d, int i){

	b.nazwa = c;
	b.waga = d;
	b.kalorie = i;

	cout << b.nazwa << endl;
	cout << b.waga << endl;
	cout << b.kalorie << endl;
}
