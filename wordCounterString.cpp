/*words counter*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <array>

using std::cout;
using std::cin;
using std::endl;

int main() {

	std::string text;
	std::string key = "gotowe";

	int counter = 0;

	cin >> text;

	while (text != key) {
		counter++;
		cin >> text;
	}

	cout << "Liczba slow: " << counter << endl;

	system("PAUSE");
	return 0;
}