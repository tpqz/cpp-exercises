/*
 * overload.cpp
 *
 *  Created on: Aug 29, 2016
 *      Author: tqpz
 */

#ifndef OPERATOR_CPP_
#define OPERATOR_CPP_

class Stonewt {

private:
  enum { Lbs_per_stn = 14 };
  int stone;
  double pds_left;
  double pounds;
public:
  Stonewt(double lbs);
  Stonewt(int stn, double lbs);
  Stonewt();
  ~Stonewt();
  void show_lbs() const;
  void show_stn() const;
  Stonewt operator*(double mult);
};

friend Stonewt operator*(double mult, const Stonewt & s);

Stonewt operator*(double mult, const Stonewt & s){
	return (mult * s.pounds);
}

Stonewt Stonewt::operator*(double mult){
	return Stonewt(mult * pounds);
}


#endif
