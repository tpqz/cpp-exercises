/*
 * main.cpp
 *  *
 *  Created on: Aug 26, 2016
 *      Author: tqpz
 */

#include "list.h"

int main(){
	using namespace std;

	List spis;
	item a;

	char ch;

	while(ch != '7'){
		cout << "------------------------------------\n";
		cout << "---------Panel sterowania-----------\n";
		cout << "------------------------------------\n";
		cout << "-- [1] Sprawdz stan listy         --\n";
		cout << "-- [2] Dodaj osobe do listy       --\n";
		cout << "-- [3] Wyswietl osobe             --\n";
		cout << "-- [4] Usun osobe z listy         --\n";
		cout << "-- [5] Pokaz wszystkie osoby      --\n";
		cout << "-- [6] Pokaz wolne miejsce        --\n";
		cout << "-- [7] Zakoncz                    --\n";
		cout << "------------------------------------\n";
		cout << "~$ ";
		cin >> ch;

	switch(ch){
	case '1':
		spis.showState();
		break;
	case '2':
		spis.add(a);
		break;
	case '3':
		spis.showElement();
		break;
	case '4':
		spis.removeElement();
		break;
	case '5':
		spis.showAll();
		break;
	case '6':
		spis.showSpace();
		break;
	case '7':
		cout << "Koniec.";
		break;
	default:
		cout << "Wybierz dozwolona opcje";
		break;
	}
}

	return 0;

}
