/*
 * list.cpp
 *
 *  Created on: Aug 26, 2016
 *      Author: tqpz
 */

#include "list.h"
#include <iterator>
#include <cstdlib>

void stop(){
	std::cout << "Wcisnij enter aby kontynuowac...\n";
	std::cin.get();
	std::cin.get();
}

List::List(){
	space=0;
	full=10;
}


void List::add(item a){
	using namespace std;

	if (space < full){
		cout << "Podaj imie i wiek: \n";
		cin >> a.name >>a.age;
		cout << endl;

		list.push_back(a);
		space += 1;

		cout << "Pomyslnie dodano osobę " << a.name << ", w wieku " << a.age << " lat.\n";
		cout << endl;
	}else{
		cout << "------------------------------------\n";
		cout << "Brak miejsca!\n";
		cout << "------------------------------------\n";

	}
	stop();
}

void List::showAll(){

	std::list<item>::const_iterator it = list.begin();
	int counter = 0;

	if(it != list.end()){
		for(it = list.begin(); it != list.end(); ++it){
			counter += 1;
			std::cout << counter <<". " <<"Imie: " <<it->name << ", wiek: "<< it->age << std::endl;
			std::cout << "------------------------------------\n";
		}
	}else
		std::cout << "Lista jest pusta!\n";

	stop();
}

void List::showState(){
	if(space < full){
		std::cout << "------------------------------------\n";
		std::cout << "Jest wolne miejsce na liscie\n";
		std::cout << "------------------------------------\n";

	}else if (space >= full){
		std::cout << "------------------------------------\n";
		std::cout << "Brak miejsca na liscie\n";
		std::cout << "------------------------------------\n";
	}
	stop();
}

void List::showElement(){
	int n;

	std::cout << "Podaj numer osoby: ";
	std::cin >> n;

	std::list<item>::const_iterator it = list.begin();

	std::advance(it, n-1);

	if(it != list.end()){
		std::cout << "------------------------------------\n";
		std::cout <<"Imie: " <<it->name << ", wiek: "<< it->age << std::endl;
		std::cout << "------------------------------------\n";
	}else
		std::cout << "Nie ma osoby o tym numerze!\n";

	stop();
}

void List::removeElement(){

	int n;
	std::list<item>::iterator it = list.begin();
	std::cout << "Podaj numer osoby na liscie: ";
	std::cin >> n;

	advance(it, n-1);

	if(it != list.end()){
		list.erase(it);
		std::cout << "------------------------------------\n";
		std::cout << "Osoba o numerze " << n << " zostala usunieta\n";
		std::cout << "------------------------------------\n";
	}else
		std::cout << "Nie ma osoby o tym numerze!\n";

	stop();
}

void List::showSpace(){
	std::cout << "------------------------------------\n";
	std::cout << "Zostalo " << full - space << " miejsc\n";
	std::cout << "------------------------------------\n";
	stop();
}


