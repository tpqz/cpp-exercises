/*
 * list.h
 *
 *  Created on: Aug 26, 2016
 *      Author: tqpz
 */

#ifndef LIST_H_
#define LIST_H_
#include <list>
#include <iostream>
#include <string>

struct Person {
	std::string name;
	int age;
};


typedef Person item;

class List{

private:
	std::list<item> list;
	int space; //free space
	int full; //capacity of list

public:
	List();
	void add(item a);
	void showAll();
	void showState();
	void showElement();
	void removeElement();
	void showSpace();
};

#endif /* LIST_H_ */
