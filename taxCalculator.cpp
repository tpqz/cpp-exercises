/*tax calculator*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

int main() {

	const double FIRST_LEVEL = 0;
	const double SECOND_LEVEL = 0.1;
	const double THIRD_LEVEL = 0.15;
	const double FOURTH_LEVEL = 0.2;

	//char input;
	double tax, income;

	cout << "Podaj wartosc swoich zarobkow: ";
	cin >> income;


	if (income <= 5000) {
		tax = income * FIRST_LEVEL;
		cout << "Podatek wynosi: " << tax << endl;
	}
	else if (income > 5000 && income <= 10000) {
		tax = (income * FIRST_LEVEL) + ((10000 - income) * SECOND_LEVEL);
		cout << "Podatek wynosi: " << tax << endl;
	}
	else if (income > 10000 && income <= 20000) {
		tax = (income * FIRST_LEVEL) + (10000 * SECOND_LEVEL) + ((20000 - income) * THIRD_LEVEL);
		cout << "Podatek wynosi: " << tax << endl;
	}
	else if (income > 20000) {
		tax = (income * FIRST_LEVEL) + (10000 * SECOND_LEVEL) + (20000 * THIRD_LEVEL) + ((income - 35000) * FOURTH_LEVEL);
		cout << "Podatek wynosi: " << tax << endl;
	}

	system("PAUSE");
	return 0;
}