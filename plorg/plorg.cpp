/*
 * plorg.cpp
 *
 *  Created on: Aug 26, 2016
 *      Author: tqpz
 */
#include <iostream>
#include <stdio.h>
#include <string.h>
#include "plorg.h"

Plorg::Plorg(){
	hunger = 0;
};

Plorg::Plorg(char n[19], int h){
	strcpy(name,n);
	hunger = h;
}

void Plorg::setHunger(int h){
	hunger = h;
}

void Plorg::show(){
	std::cout << "Imie plorga: " << name << std::endl;
	std::cout << "Poziom glodu: " << hunger << std::endl;
}
