/*
 * main.cpp
 *
 *  Created on: Aug 26, 2016
 *      Author: tqpz
 */

#include "plorg.h"

int main(){

	Plorg test("Jan", 21);
	Plorg pusty;

	test.show();
	pusty.show();
	pusty.setHunger(50);
	pusty.show();

	return 0;
}
