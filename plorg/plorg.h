/*
 * plorg.h
 *
 *  Created on: Aug 26, 2016
 *      Author: tqpz
 */

class Plorg{

private:
	char name[19] = "Plorga";
	int hunger;

public:
	Plorg();
	Plorg(char n[19], int h);
	void setHunger(int h);
	void show();
};


