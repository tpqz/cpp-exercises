/*calculate*/

#include "stdafx.h"
#include <iostream>
#include <cstdlib>

using namespace std;

double add(double x, double y);
double calculate(double x, double y, double(*pf)(double x, double y));
double mul(double x, double y);

int main() {
	double x, y;

	cout << "Podaj liczby x i y: ";
	cin >> x >> y;
	calculate(x, y, add);
	calculate(x, y, mul);

	system("PAUSE");
	return 0;
}

double add(double x, double y) {
	return x + y;
}

double mul(double x, double y) {
	return x * y;
}

double calculate(double x, double y, double(*pf)(double x, double y)) {
	double result = pf(x, y);
	cout << result << endl;
	return result;
}