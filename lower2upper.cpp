/*upper2lower/lower2upper*/

#include "stdafx.h"
#include <iostream>
#include <cctype>


using std::cout;
using std::cin;
using std::endl;

int main() {

	char ch;
	char upp;
	char low;
	int counter;

	while (cin.get(ch) && !(isdigit(ch))) {
		if (islower(ch)) {
			upp = toupper(ch);
			cout << upp;
		}
		else if (isupper(ch)) {
			low = tolower(ch);
			cout << low;
		}
		else if (isblank(ch)) {
			cout << ch;
		}
	}

	system("PAUSE");
	return 0;
}