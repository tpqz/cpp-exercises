/*
 * nmspace.cpp
 *
 *  Created on: Aug 20, 2016
 *      Author: tqpz
 */
#include <iostream>

int main(){
	double x;
	std::cout << "Podaj wartsoc: ";
	while (! (std::cin >> x)){
		std::cout << "Nalezy wprowadzic liczbe :";
		std::cin.clear();
		while(std::cin.get() != '\n')
			continue;
	}
	std::cout << "Podano " << x << std::endl;
	return 0;
}


