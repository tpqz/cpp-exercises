/*words counter*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <array>

using std::cout;
using std::cin;
using std::endl;

int main() {

	char text[1000];
	char key[] = "gotowe";

	int counter = 0;

	cin >> text;

	while (strcmp(text, key) != 0) {
		counter++;
		cin >> text;
	}

	cout << "Liczba slow: " << counter << endl;

	system("PAUSE");
	return 0;
}