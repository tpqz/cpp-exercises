/*
 * sales.h

 *
 *  Created on: Aug 23, 2016
 *      Author: tqpz
 */


namespace SALES{
	const int QUATERS = 4;

	struct Sales{
		double sales[QUATERS];
		double average;
		double max;
		double min;
	};

	void setSales(Sales & s, const double ar[], int n);
	void setSales(Sales & s);
	void showSales(Sales & s);
}
