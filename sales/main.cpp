/*
 * main.cpp
 *
 *  Created on: Aug 23, 2016
 *      Author: tqpz
 */

#include <iostream>
#include "sales.h"

int main(){

	/*
	SALES::Sales s ={
		{2,4,1,7};
		4;
		4;
		4;
	};
*/
	SALES::Sales s = {{12,7,5,2}};
	SALES::Sales a = {{124,723,45,22}};

	SALES::setSales(s);
	std::cout << std::endl;
	SALES::setSales(a);
	SALES::showSales(s);
	SALES::showSales(a);
	return 0;
}
