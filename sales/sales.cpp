/*
 * sales.cpp
 *
 *  Created on: Aug 23, 2016
 *      Author: tqpz
 */

#include <iostream>
#include "sales.h"
#include <vector>

void setSales(SALES::Sales & s, std::vector<double> ar, int n){

	double total = 0;
	double avg = 0;
	double max = 0;
	double min = 0;

	for (int i = 0; i < 4; i++){

		if (ar[i] < min)
			min = ar[i];
	}

	s.min = min;


	for (int i = 0; i < 4; i++){

		if (ar[i] > max)
		max = ar[i];
	}

	s.max = max;

	for (int i = 0; i < 4; i++){
		total += ar[i];
		avg = total/4;
		s.average = avg;
	}

	//s.sales = ar;
}


void SALES::setSales(SALES::Sales & s){

	double total = 0;
	double avg = 0;
	double max = 0;
	double min = 512;

	for (int i = 0; i < 4; i++){
		if (s.sales[i] > max){
			max = s.sales[i];
		}

	}

	s.max = max;

	for (int i = 0; i < 4; i++){
		if (s.sales[i] < min){
			min = s.sales[i];

		}

	}

	s.min = min;


	for (int i = 0; i < 4; i++){
			total += s.sales[i];
			avg = total/4;
			s.average = avg;
	}
}

void SALES::showSales(SALES::Sales & s){
	std::cout << "Najwyzsza wartosc: " << s.max <<std::endl;
	std::cout << "Najnizsza wartosc: " << s.min <<std::endl;
	std::cout << "Srednia wartosc: " << s.average <<std::endl;
}



