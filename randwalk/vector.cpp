/*
 * vector.cpp
 *
 *  Created on: Aug 29, 2016
 *      Author: tqpz
 *
 * Save result to file
 */

#include <iostream>
#include <cstdlib>      // rand(), srand() prototypes
#include <ctime>        // time() prototype
#include <fstream>
#include "vect.h"

int main()
{
    using namespace std;
    using VECTOR::Vector;
    srand(time(0));     // seed random-number generator
    double direction;
    Vector step;
    Vector result(0.0, 0.0);
    unsigned long steps = 0;
    double target;
    double dstep;
    ofstream file;

    file.open("src/vectorResult.txt");

    cout << "Enter target distance (q to quit): ";
    while (cin >> target)
    {
        cout << "Enter step length: ";
        if (!(cin >> dstep))
            break;

        file << "Distance to target: " << target << ", step length: " << dstep << endl;
        while (result.magval() < target)
        {
            direction = rand() % 360;
            step.set(dstep, direction, 'p');
            result = result + step;
            file << steps + 1 << ". " << result << endl;
            steps++;
        }
        file << "After " << steps << " steps, the subject "
            "has the following location:\n";
        file << result << endl;
        result.polar_mode();
        file << " or\n" << result << endl;
        file << "Average outward distance per step = "
            << result.magval()/steps << "\n\n"<< endl;

        steps = 0;
        result.set(0.0, 0.0);
        cout << "Enter target distance (q to quit): ";
    }
    cout << "Bye!\n";
    file.close();

    return 0;
}
