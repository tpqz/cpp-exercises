#include <iostream>

using namespace std;

template <class T>
T max(T  a[5], T  len){

	if (len > sizeof(a)){
		cout << "error!";
		return 0;
	}

	T biggest = 0;

	for(int i=0; i<sizeof(a); i++){

		if(a[i] > biggest){
			biggest = a[i];
		}

		if (i == len-1){
			cout << "Najwieksza: " << biggest;
		}
	}
}

int main() {

	int x[5] = {9,13,421,6412,52};
	double y[4] = {1.4,22.1,3.52,4.8};
	float z[6] = {231.4,445123.2,1231.2,123155.2221,4123.32,5.7};

	max(x, 6);

return 0;
}
