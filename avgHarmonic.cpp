/*average harmonic*/

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

double avge(double x, double y) {
	double result = 2 * x*y / (x + y);
	cout << "Srednia harmoniczna: " << result << endl;
	return result;
}

int main() {

	double x = 0, y = 0;

	cout << "Podaj dwie liczby: ";
	cin >> x >> y;
	if (x == 0 || y == 0) {
		exit;
	}
	else {
		avge(x, y);
	}

	while (x != 0 && y != 0) {
		if (x == 0 || y == 0) {
			break;
			exit;
		}
		else {
			cout << "Podaj dwie liczby: ";
			cin >> x >> y;
			avge(x, y);
		}
	}


	system("PAUSE");
	return 0;
}

