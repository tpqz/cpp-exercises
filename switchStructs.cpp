/*switch, array of structs*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

int main() {

	char choice;

	struct monk {
		char name[256];
		char position[256];
		char nickname[256];
		int prefer; // 0 = name, 1=position, 2=nickname
	};

	monk list[4]{
		{ "Mati", "Senior", "tpqz",2 },
		{ "Jan", "Junior", "edes",1 },
		{ "Stefan", "Tester", "teet",0 },
		{ "Henryk", "FrontEnd", "Fec",1 }
	};

	cout << "Zakon Programistow Dobrej Woli\na. lista imion\t\tb. lista wg stanowisk\nc. lista wg pseudonimow\td. lista wg preferencji"
		"\nq. koniec\nCo wybierasz?: ";
	cin >> choice;

	while (choice != 'q') {
		switch (choice) {

		case 'A':
		case 'a':
			cout << endl;
			for (int i = 0; i < 4; i++) {
				cout << list[i].name << endl;
			}
			cout << endl;
			break;

		case 'B':
		case 'b':
			cout << endl;
			for (int i = 0; i < 4; i++) {
				cout << list[i].position << endl;
			}
			cout << endl;
			break;

		case 'c':
		case 'C':
			cout << endl;
			for (int i = 0; i < 4; i++) {
				cout << list[i].nickname << endl;
			}
			cout << endl;
			break;

		case 'd':
		case 'D':
			cout << endl;
			for (int i = 0; i < 4; i++) {
				if (list[i].prefer == 0) {
					cout << list[i].name << endl;
				}
				else if (list[i].prefer == 1) {
					cout << list[i].position << endl;
				}
				else if (list[i].prefer == 2) {
					cout << list[i].nickname << endl;
				}
			}
			cout << endl;
			break;
		default: break;
		}

		cout << "Zakon Programistow Dobrej Woli\na. lista imion\t\tb. lista wg stanowisk\nc. lista wg pseudonimow\td. lista wg preferencji"
			"\nq. koniec\nCo wybierasz?: ";
		cin >> choice;
	}

	system("PAUSE");
	return 0;
}