/*input sum*/

#include "stdafx.h"
#include <iostream>
#include <array>
#include <iomanip>


using namespace std;
const int ARSIZE = 100;

int main() {
	int i = 0;
	int add = 1;

	while (add != 0) {
		cout << "Podaj liczbe :";
		cin >> add;
		i += add;
		cout << "Suma: " << i << endl;
	}

	system("PAUSE");
	return 0;
}