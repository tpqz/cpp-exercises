/*FUNCTIONS*/

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;
const int SIZE = 10;

void fillScore(int scores[], int size) {
	cout << "Aby zakonczyc wczytywanie podaj liczbe 0\n";
	int score;
	for (int i = 0; i < size; i++) {
		cout << "Podaj wynik nr" << i + 1 << ": ";
		cin >> score;
		if (score != 0) {
			scores[i] = score;
		}
		else {
			break;
		}
	}
}

void showScore(int scores[], int size) {
	for (int i = 0; i < size; i++) {
		if (scores[i] > 0) {
			cout << "Wynik nr" << i + 1 << ": " << scores[i] << endl;
		}
		else {
			break;
		}
	}
}

void avgScore(int scores[], int size) {
	int sum = 0;
	double result = 0;
	double counter = 0;

	for (int i = 0; i < size; i++) {

		if (scores[i] > 0) {
			counter++;
			sum += scores[i];
		}
		else {
			break;
		}
	}

	result = (double)sum / counter;
	cout << "Srednia: " << fixed << setprecision(2) << round(result) << endl;
}

int main() {

	void fillScore(int scores[], int size);
	void showScore(int scores[], int size);
	void avgScore(int scores[], int size);

	int scores[SIZE];

	fillScore(scores, SIZE);
	showScore(scores, SIZE);
	avgScore(scores, SIZE);

	system("PAUSE");
	return 0;
}

