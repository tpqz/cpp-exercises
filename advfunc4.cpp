/*
 * advfunc4.cpp
 *
 *  Created on: Aug 19, 2016
 *      Author: tqpz
 */

#include <iostream>
#include <cstring>

using namespace std;

struct stringy{
	char * str;
	int ct;
};

void set(stringy & s, char * ch);
void show(char * st);
void show(char * st, int num);
void show(stringy strct);
void show(stringy strct, int cnt);

int main(){
	stringy beany;

	char testing[] = "Rzeczywistosc to juz nie to, co kiedys";

	set(beany, testing);
	show(beany);
	show(beany, 2);
	testing[0]= 'D';
	testing[1]= 'u';
	show(testing);
	show(testing, 3);
	show("Gotowe!");
	return 0;
}

void set(stringy & s, char * ch){
	s.str = ch;
}

void show(char * st){
	cout << st <<endl;
}

void show(char * st, int count){
	for(int i=0;i<=count; i++){
		cout << st <<endl;
	}
}

void show(stringy strct){
	cout << strct.str <<endl;
}

void show(stringy strct, int cnt){
	for(int i=0;i<=cnt; i++){
		cout << strct.str <<endl;
	}
}
