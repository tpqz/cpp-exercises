#include "stdafx.h"
#include <iostream>
double cm2foot(double);
double cm2inch(double);

const double inch = 0.3937;
const double foot = 0.0328;

int main() {
	double height;

	std::cout << "Podaj wzrost w cm: " << std::endl;
	std::cin >> height;

	double inches = cm2inch(height);
	double feets = cm2foot(height);

	std::cout << "Wzrost w stopach : " << feets << std::endl;
	std::cout << "Wzrost w calach : " << inches << std::endl;

	std::cin.get();
	std::cin.get();
	return 0;
}

double cm2foot(double cm) {
	return cm*foot;
}

double cm2inch(double cm) {
	return cm*inch;
}
