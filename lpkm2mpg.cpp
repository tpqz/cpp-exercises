/*converting l/100km to mpg*/

#include "stdafx.h"
#include <iostream>
#include <iomanip>

int main() {
	double liter, gallon, mile, gallonPerMile;
	
	std::cout << "Podaj zuzycie benzyny na 100km: ";
	std::cin >> liter;

	mile = 62.14;
	gallon = liter*0.2642;

	gallonPerMile = mile / gallon;

	std::cout << std::setprecision(3) << gallonPerMile;

	std::cin.get();
	std::cin.get();

	return 0;
}