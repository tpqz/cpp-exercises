/*
 * golf.h
 *
 *  Created on: Aug 22, 2016
 *      Author: tqpz
 */
#include <string>

struct golf {
	std::string fullname;
	int handicap;
};

void setGolf(golf & g, std::string name, int hc);
void setGolf(golf & g);
void handicap(golf & g, int hc);
void showGolf(const golf & g);
