/*
 * golf.cpp
 *
 *  Created on: Aug 22, 2016
 *      Author: tqpz
 */
#include <iostream>
#include "golf.h"

using std::cin;
using std::endl;
using std::cout;


void setGolf(golf & g, std::string name, int hc){
	g.fullname = name;
	g.handicap = hc;
}

void setGolf(golf & g){
	cout << "Podaj imie" << endl;
		cin >> g.fullname;

	cout << "Podaj handicap" << endl;
		cin >> g.handicap;
}

void handicap(golf & g, int hc){
	g.handicap = hc;
	cout << "Handicap ustawiony na: " << g.handicap << endl;
}

void showGolf(const golf & g){
	cout << "Nazwisko: " << g.fullname << "\nPoziom: " << g.handicap << endl;
}
