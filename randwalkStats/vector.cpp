/*
 * vector.cpp
 *
 *  Created on: Aug 29, 2016
 *      Author: tqpz
 *
 * deleted in private class variables mag,ang.
 * file vector.cpp untouched
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>      // rand(), srand() prototypes
#include <ctime>        // time() prototype
#include <fstream>
#include "vect.h"

int main()
{
    using namespace std;
    using VECTOR::Vector;
    srand(time(0));     // seed random-number generator
    double direction;
    Vector step;
    Vector result(0.0, 0.0);
    unsigned long steps = 0;
    double target;
    double dstep;
    int attempts;
    unsigned long stepCounter = 0;
    unsigned long minSteps;
    unsigned long maxSteps;
    double avgSteps;

    cout << "Enter target distance (q to quit): ";
    while (cin >> target)
    {
        cout << "Enter step length: ";
        if (!(cin >> dstep))
            break;

        cout << "Enter number of attempts: ";
        	cin >> attempts;

        for (int i = 0; i <= attempts-1; i++){
        while (result.magval() < target)
        {
            direction = rand() % 360;
            step.set(dstep, direction, 'p');
            result = result + step;
            steps++;
        }


        stepCounter += steps;

        if (i == 0 || steps <= minSteps)
        	minSteps = steps;


        if (i == 0 || steps >= maxSteps)
        	maxSteps = steps;


        avgSteps =  (double)stepCounter / (double)attempts;

        steps = 0;
        result.set(0.0, 0.0);

        }

        std::cout.setf( std::ios::fixed, std:: ios::floatfield );
        cout << "Number of all steps: " << stepCounter  << endl;
        cout << "Number of attempts: " << attempts << endl;
        cout << "Least steps of all attempts: " << minSteps << endl;
        cout << "Most steps of all attempts: " << maxSteps << endl;
        cout << "Average steps per one attempt: "<< setprecision(2) << avgSteps << endl;
        cout << "Enter target distance (q to quit): ";
        /*
        minSteps = 0;
        maxSteps = 0;
        avgSteps = 0;
        stepCounter = 0;
        */
    }
    cout << "Bye!\n";

    cin.clear();

    return 0;
}
