#include "stdafx.h"
#include <iostream>

const double Inch = 12;
const double Meters = 0.0254;
const double Kg = 2.2;

int main() {
	double feets;
	double pounds;

	std::cout << "Podaj wzrost w stopach: ";
	std::cin >> feets;
	std::cout << "Podaj wag� w funtach: ";
	std::cin >> pounds;

	double inches = feets * 12;
	double meters = inches*0.0254;
	double kgs = pounds / 2.2;

	double bmi = kgs / pow(meters, 2);

	std::cout << "Twoje bmi: " << bmi << std::endl;

	std::cin.get();
	std::cin.get();
	return 0;
}