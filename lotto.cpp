/*lotto win probablity*/

#include "stdafx.h"
#include <iostream>
#include <cstdlib>

using namespace std;
long double probability(double numbers, double picks);
long double superNumber(double number);

int main() {

	double total, choices, superNums;

	cout << "Podaj najwieksza liczbe jaka mozesz wybrac,\n"
		"liczbe skreslen, oraz najwieksza superliczbe:";

	while ((cin >> total >> choices >> superNums) && choices <= total) {
		cout << "Szansa wygranej to jeden do: ";
		cout << probability(total, choices) << endl;
		cout << "Szansa na traf superliczby: " << superNumber(superNums) << endl;
		cout << "Szansa na wygrana: " << probability(total, choices) * superNumber(superNums) << endl;
		cout << endl;
		cout << "Nastepne trzy liczby (q, aby zakonczyc): ";
	}
	cout << "Do widzenia\n";

	system("PAUSE");
	return 0;
}

long double probability(double numbers, double picks) {
	long double result = 1.0;
	long double n;
	double p;

	for (n = numbers, p = picks; p > 0; n--, p--) {
		result = (result * n) / p;
	}
	return result;
}

long double superNumber(double number) {
	long double result = 1.0 / number;
	return result;
}