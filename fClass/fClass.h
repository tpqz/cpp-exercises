/*
 * fClass.h
 *
 *  Created on: Aug 25, 2016
 *      Author: tqpz
 */

#ifndef FCLASS_H_
#define FCLASS_H_
#include <string>

class Account {

private:
	std::string name;
	std::string accnum;
	double money;

public:

	Account();
	Account (std::string nme, std::string num, double mn = 0.0);
	~Account();
	void show() const;
	void deposit(double mn);
	void withdraw(double mn);
};

#endif
