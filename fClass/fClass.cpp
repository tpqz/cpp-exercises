/*
 * fClass.cpp
 *
 *  Created on: Aug 25, 2016
 *      Author: tqpz
 */

#include <iostream>
#include <iomanip>      // std::setprecision
#include "fClass.h"

Account::Account(){
	name = "CHUJ";
	accnum = "0";
	money = 0.0;
}

Account::Account (std::string nme, std::string num, double mn){
	name = nme;
	accnum = num;
	money = mn;
};

Account::~Account(){
	std::cout << "Koniec.\n";
};

void Account::show() const{

	std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);

	std::cout.precision(2);

	std::cout << "Nazwisko depozytariusza: " << Account::name << std::endl;
	std::cout << "Stan konta: " << Account::money << ", ";
	std::cout << "Numer konta: " << Account::accnum << std::endl;
}

void Account::deposit(double mn){
	money += mn;
	std::cout << "Wplacono: " << mn << "zl\n";
}

void Account::withdraw(double mn){
	money -= mn;
	std::cout << "Wyplacono: " << mn << "zl\n";
}
