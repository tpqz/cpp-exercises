/*
 * mainC.cpp
 *
 *  Created on: Aug 25, 2016
 *      Author: tqpz
 */

#include <iostream>
#include "fClass.h"

int main(){

	Account first("Tapa","41231222", 10000.51);

	first.show();

	first.deposit(2041.2445);
	first.show();

	first.withdraw(10.2111);
	first.show();


return 0;
}
