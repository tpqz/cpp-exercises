/*
 * nmspace2.cpp
 *
 *  Created on: Aug 23, 2016
 *      Author: tqpz
 */

#include <iostream>
#include <cstring>

int main(){

	struct chaff{
		char dross[20];
		int slag;
	};

	char buffer[512];

	chaff * ch1 = new chaff[2];

	strcpy(ch1[0].dross, "saes");
		ch1[0].slag = 2;

	strcpy(ch1[1].dross, "chuj");
		ch1[1].slag = 2241;

	for (int i = 0; i < 2; i++){
		std::cout << ch1[i].dross << " adress: " << &ch1[i].dross << std::endl;
		std::cout << ch1[i].slag << " adress: " << &ch1[i].slag << std::endl;
	}

	return 0;
}
