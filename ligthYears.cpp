// BookApps.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

double astroUnit(double);

int main() {
	double lightYear;
	std::cout << "Podaj liczbe lat swietlnych: " << std::endl;
	std::cin >> lightYear;

	double astrUnit = astroUnit(lightYear);

	std::cout << lightYear << " lat swietlnych to " << astrUnit << " jednostek astronomicznych" << std::endl;

	std::cin.get();
	std::cin.get();

	return 0;
}

double astroUnit(double lightYear) {
	return lightYear * 63240;
}