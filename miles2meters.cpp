// BookApps.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
int miles2meters(int);

int main(){
	int miles;

		std::cout << "Podaj odleg�osc w milach morskich" << std::endl;
		std::cin >> miles;
		int meters = miles2meters(miles);

		std::cout << miles << " mil morskich to " << meters << " metrow " << std::endl;

		std::cin.get();
		std::cin.get();
		return 0;
}

int miles2meters(int mls) {
	return mls * 1852;
}
