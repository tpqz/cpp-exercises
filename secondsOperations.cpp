#include "stdafx.h"
#include <iostream>

const int HOURS = 24;
const int MINUTES = 60;
const int SECONDS = 60;
const int SECINDAY = HOURS*MINUTES*SECONDS;

int main() {
	long secondsInput;
	int days;
	int hours;
	int minutes;
	int seconds;

	std::cout << "Podaj liczbe sekund: ";
	std::cin >> secondsInput;

	days = secondsInput / SECINDAY;
	hours = (secondsInput % SECINDAY) / (MINUTES*SECONDS);
	minutes = (secondsInput % SECINDAY) % (MINUTES*SECONDS) / MINUTES;
	seconds = (secondsInput % SECINDAY) % (MINUTES*SECONDS) % MINUTES;

	std::cout << secondsInput << " sekund = " << days << " dni, " << hours << " godzin, " << minutes << " minut, " << seconds << " sekund. \n";

	std::cin.get();
	std::cin.get();
	return 0;
}