/*replacement char[] to string*/

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

int main() {

	string name;
	string dessert;

	cout << "Podaj swoje imie: ";
	getline(cin, name);

	cout << "Podaj swoj ulubiony deser: ";
	getline(cin, dessert);

	cout << "Mam dla ciebie: " << dessert << ", " << name << endl;

	system("PAUSE");
	return 0;
}