/*
 * advfunc4.cpp
 *
 *  Created on: Aug 19, 2016
 *      Author: tqpz
 */

#include <iostream>

using namespace std;

template <class T>
T fun(T a[5]){

	T biggest = 0;

	for(int i=0; i<5; i++){

		if(a[i] > biggest){
			biggest = a[i];
		}

		if (i == 4){
			cout << "Najwieksza: " << biggest;
		}
	}
}

int main() {

	int x[5] = {9,13,421,6,52};
	double y[5] = {1.4,22.1,3.52,4.8,5.7};
	float z[5] = {231.4,445123.2,1231.2,123155.2221,4123.32};

	fun(z);

return 0;
}

