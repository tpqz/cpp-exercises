#include "stdafx.h"
#include <iostream>

int main() {
	long long worldPopulation;
	long long plPopulation;

	std::cout << "Podaj liczbe ludnosci swiata: ";
	std::cin >> worldPopulation;

	std::cout << "Podaj liczbe ludnosci polski: ";
	std::cin >> plPopulation;

	double percent = (100 * (double)plPopulation) / (double)worldPopulation;

	std::cout << "Populacja stanowi " << percent << "% populacji swiata. \n";

	std::cin.get();
	std::cin.get();
	return 0;
}