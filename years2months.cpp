// BookApps.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
int years2months(int);

int main() {
	int years;

	std::cout << "Podaj wiek" << std::endl;
	std::cin >> years;

	int months = years2months(years);

	std::cout << "Twoj wiek w miesiacach: " << months << std::endl;

	std::cin.get();
	std::cin.get();
}

int years2months(int years) {
	return years * 12;
}