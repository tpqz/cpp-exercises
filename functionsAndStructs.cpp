/*functions&struct*/

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;


struct pudlo {
	char producent[40];
	float wysokosc;
	float szerokosc;
	float dlugosc;
	float objetosc;
};

void display(pudlo x) {
	cout << "Producent: " << x.producent << endl;
	cout << "Wysokosc: " << x.wysokosc << endl;
	cout << "Szerokosc: " << x.szerokosc << endl;
	cout << "Dlugosc: " << x.dlugosc << endl;
	cout << "Objetosc producenta: " << x.objetosc << endl;
}

void action(pudlo * x) {
	x->objetosc = x->wysokosc * x->dlugosc * x->szerokosc;
	cout << "Objetosc realna: " << x->objetosc << endl;
}

int main() {

	void display(pudlo x);

	pudlo samsung{
		"Samsung",
		10,
		15,
		30,
		100,
	};

	display(samsung);
	action(&samsung);

	system("PAUSE");
	return 0;
}

