/*
 * file2.cpp
 *
 *  Created on: Aug 20, 2016
 *      Author: tqpz
 */

#include <iostream>
using namespace std;

void other();

extern int x;

namespace
{
	int y = 4;
}

void another(){
	cout << "Funkcja another(): " << x << ", " << y << endl;
}

