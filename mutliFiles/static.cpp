/*
 * static.cpp
 *
 *  Created on: Aug 22, 2016
 *      Author: tqpz
 */

#include <iostream>
#include <string>
#include <algorithm>

void strcount(std::string str);

int main(){
	using namespace std;

	string input;
	string next;
	string end = "exit";


	cout << "Wprowadz wiersz:\n";

		while(input.compare(end)){
			getline(cin,input);
			input.erase(remove(input.begin(), input.end(), ' '), input.end());
			strcount(input);
			cout << "Wprowadz nastepny wiersz (exit konczy):\n";
		}
	return 0;
}

void strcount(std::string str){
	using namespace std;

	static int total = 0;
	int count = 0;
	string end = "exit";

	if(!str.compare(end)){
		cout << "Koniec\n";
		exit (EXIT_FAILURE);
	}

	cout << "\"" << str << "\" zawiera ";

	for (int i = 0; i < str.size(); i++){
		count += 1;
	}

	 total += count;
	cout << count <<" znakow\n";
	cout << "Lacznie " << total <<" znakow\n";

}
