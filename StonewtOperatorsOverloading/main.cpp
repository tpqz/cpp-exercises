/*
 * main.cpp
 *
 *  Created on: Aug 29, 2016
 *      Author: tqpz
 */

#include "stonewt.h"
using std::cout;

int main(){

	Stonewt test(225.5);
	Stonewt test2(213,225.5);
	Stonewt test3;

	cout << test;
	cout << test2;

	test3 = test * test2;
	cout << test3;

	return 0;
}
