/*
 * stonewt.cpp
 *
 *  Created on: Aug 29, 2016
 *      Author: tqpz
 */

//#include <iostream>
#include "stonewt.h"
using std::cout;

// construct stonewt object from double value
Stonewt::Stonewt(double lbs)
{
	stone = int(lbs) / Lbs_per_stn;		// integer division
	pds_left = int(lbs) % Lbs_per_stn + lbs - int(lbs);
	pounds = lbs;
}

// construct Stonewt object from stone, double values
Stonewt::Stonewt(int stn, double lbs)
{
	stone = stn;
	pds_left = lbs;
	pounds = stn * Lbs_per_stn + lbs;
}

Stonewt::Stonewt()		// default constructor, wt = 0
{
	stone = pounds = pds_left = 0;
}

Stonewt::~Stonewt()		// destructor
{
}

// show weight in stones
void Stonewt::show_stn() const
{
	cout << stone << " stone, " << pds_left << " pounds \n";
}

void Stonewt::show_lbs() const
{
	cout << pounds << " pounds\n";
}

ostream & operator<<(ostream & os, const Stonewt & st){
	os << st.stone << " stone" << ", " << st.pounds << " pounds\n";
	return os;
}


Stonewt Stonewt::operator+(const Stonewt & st) const{
	Stonewt temp;
	temp.pounds = st.pounds + pounds;
	temp.stone = st.stone + stone;
	return temp;
}

Stonewt Stonewt::operator-(const Stonewt & st) const{
	Stonewt temp;
	temp.pounds = st.pounds - pounds;
	temp.stone = st.stone - stone;
	return temp;
}

Stonewt Stonewt::operator*(const Stonewt & st) const{
	Stonewt temp;
	temp.pounds = st.pounds * pounds;
	temp.stone = st.stone * stone;
	return temp;
}



