// BookApps.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

int degrees(int);

int main() {
	int celsius;
	std::cout << "Podaj temperature w stopniach Celsjusza: " << std::endl;
	std::cin >> celsius;

	int fahrenheit = degrees(celsius);

	std::cout << celsius << " stopni Celsjusza to " << fahrenheit << " stopni Fahrenheita" << std::endl;

	std::cin.get();
	std::cin.get();

	return 0;
}

int degrees(int celsius) {
	return ((9 * celsius) / 5) + 32;
}