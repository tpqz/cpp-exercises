/*factorial function*/

#include "stdafx.h"
#include <iostream>
#include <cstdlib>

using namespace std;
void factorial();

int main() {

	factorial();

	system("PAUSE");
	return 0;
}

void factorial() {
	unsigned long long int x;
	unsigned long long int sum = 1;
	cout << "Podaj liczbe ('q' aby zakonczyc): ";
	while (cin >> x) {

		for (int i = x; i > 0; i--) {
			sum *= i;
		}
		cout << "Silnia z " << x << " = " << sum << endl;
		cout << "Podaj kolejna liczbe: ";
		sum = 1;
	}
}