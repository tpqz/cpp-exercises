#include "stdafx.h"
#include <iostream>

const double Degree = 1;
const double Minute = 60;
const double Second = 3600;

int main() {
	int degrees;
	int minutes;
	int seconds;

	std::cout << "Podaj dlugosc w stopniach: ";
	std::cin >> degrees;

	std::cout << "Podaj dlugosc w minutach: ";
	std::cin >> minutes;

	std::cout << "Podaj dlugosc w sekundach: ";
	std::cin >> seconds;

	double decimal = seconds / Second + minutes / Minute + degrees / Degree;

	std::cout << degrees << " stopni, " << minutes << " minut, " << seconds << " sekund = " << decimal << " stopni" << std::endl;

	std::cin.get();
	std::cin.get();
	return 0;
}