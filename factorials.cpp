/*100 factorials*/

#include "stdafx.h"
#include <iostream>
#include <array>
#include <iomanip>


using namespace std;
const int ARSIZE = 100;

int main() {

	array<long double, ARSIZE> factorials;
	factorials[1] = factorials[0] = 1LL;

	for (int i = 2; i < ARSIZE; i++) {
		factorials[i] = i * factorials[i - 1];
	}

	for (int i = 0; i < ARSIZE; i++) {
		cout << i << "! = " << factorials[i] << endl;
	}

	system("PAUSE");
	return 0;
}