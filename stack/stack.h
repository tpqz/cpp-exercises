/*
 * stack.h
 *
 *  Created on: Aug 25, 2016
 *      Author: tqpz
 */

#ifndef STACK_H_
#define STACK_H_

struct Customer {
	char fullname[45];
	double payment;
};

typedef Customer Item;

class Stack {

private:
	enum { MAX = 10 };
	Item items[MAX];
	int top;
	double bill;

public:
	Stack();
	bool isempty() const;
	bool isfull() const;
	bool push(const Item & item);
	bool pop(Item & item, double b);
	void showStack() const;
	void showBill() const;
};

#endif
