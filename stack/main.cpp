#include "stack.h"
#include <string>
#include <iostream>

int main(){

	using namespace std;

	Stack stack;
	Item c;

	int ch;

while(ch != 5){
	cout << "Co chcesz zrobic?\n";
	cout << "[1] Sprawdz stan stosu\n";
	cout << "[2] Dodaj klienta na stos\n";
	cout << "[3] Sciagnij klienta ze stosu\n";
	cout << "[4] Sprawdz stan rachunku\n";
	cout << "[5] Zakoncz\n";

	cin >> ch;

	switch (ch){

	case 1:
		stack.showStack();
		break;

	case 2:
		if (stack.isfull() == false){
			cout << "Podaj nazwę klienta oraz rachunek: ";
			cin >> c.fullname >> c.payment;
			stack.push(c);
		}else{
			cout << "Stos pełny!\n";
		}
		break;

	case 3:
		if (stack.isempty() == false){
			stack.pop(c, c.payment);
			cout << "Klient " << c.fullname << " zostal sciagniety ze stosu\n";
			cout << "Do rachunku dodano: " << (double)c.payment << "zl.\n";
		}else{
			cout << "Stos pusty\n";
		}
		break;

	case 4:
		stack.showBill();
		break;

	case 5:
		cout << "Koniec.";
		break;

	default:
		cout << "Podaj wymagany numer\n";

	}
}

return 0;

}
