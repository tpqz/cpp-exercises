/*
 * stack.cpp
 *
 *  Created on: Aug 25, 2016
 *      Author: tqpz
 */

#include "stack.h"
#include <iostream>

Stack::Stack(){
	top = 0;
	bill = 0;
}

bool Stack::isempty() const{
	return top == 0;
}

bool Stack::isfull() const{
	return top == MAX;
}

bool Stack::push(const Item & item){
	if (top < MAX){
		items[top++] = item;
		return true;
	}else{
		return false;
	}
}

bool Stack::pop(Item & item, double b){
	if (top > 0){
		item = items[--top];
		bill += b;
		return true;
	}else{
		return false;
	}
}

void Stack::showStack() const{
	std::cout << "Przedmiotow na stosie: "<< top << std::endl;
}

void Stack::showBill() const{
	std::cout << "Rachunek wynosi: " << bill << std::endl;
}
