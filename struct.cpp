/*first struct*/

#include "stdafx.h"
#include <iostream>
#include <string>


using namespace std;

int main() {

	struct batonik {
		string marka;
		double waga;
		int kcal;
	};

	batonik snack{ "Mocha munch", 2.3, 350 };

	cout << "Marka: " << snack.marka << endl;
	cout << "Waga: " << snack.waga << endl;
	cout << "Kalorie : " << snack.kcal << endl;

	system("PAUSE");
	return 0;
}