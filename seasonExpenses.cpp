/*season expenses*/

#include "stdafx.h"
#include <iostream>
#include <cstdlib>

using namespace std;

const int SEASONS = 4;
const char * SNAMES[SEASONS] = { "Wiosna", "Lato", "Jesien", "Zima" };

void fill(double * pa[SEASONS]);
void show(double da[SEASONS]);

int main() {

	double * expenses = new double[SEASONS];
	fill(&expenses);
	show(expenses);

	system("PAUSE");
	return 0;
}

void fill(double * pa[SEASONS]) {
	for (int i = 0; i < SEASONS; i++) {
		cout << "Podaj wydatki za okres >> " << SNAMES[i] << "<<: ";
		cin >> (*pa)[i];
	}
}

void show(double da[SEASONS]) {
	double total = 0.0;
	cout << "\nWydatki\n";
	for (int i = 0; i < SEASONS; i++) {
		cout << SNAMES[i] << ": " << da[i] << " zl" << endl;
		total += da[i];
	}
	cout << "Lacznie wydatki roczne: " << total << " zl" << endl;
}