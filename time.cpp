// BookApps.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
void hoursA(int);
void minutesA(int);

int main() {
	int hours;
	int minutes;

	std::cout << "Podaj liczb� godzin: " << std::endl;
	std::cin >> hours;

	std::cout << "Podaj liczb� minut: " << std::endl;
	std::cin >> minutes;

	hoursA(hours);
	minutesA(minutes);

	std::cin.get();
	std::cin.get();

	return 0;
}

void hoursA(int hours) {
	std::cout << "Czas: " << hours << ":";
}

void minutesA(int minutes) {
	std::cout << minutes << std::endl;
}