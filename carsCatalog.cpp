/*cars catalog*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <array>

using std::cout;
using std::cin;
using std::endl;

int main() {

	int carsQuant;

	struct car {
		std::string brand;
		int year;
	};

	cout << "Ile aut chcesz katalogowac?: ";
	cin >> carsQuant;
	cout << endl;
	cin.ignore(1000, '\n');

	car * cars;
	cars = new car[carsQuant];

	for (int i = 0; i < carsQuant; i++) {
		cin.clear();

		cout << endl;
		cout << "Samochod #" << i + 1 << endl;

		cout << "Podaj marke: ";
		getline(cin, (cars[i].brand));
		cin.ignore(1000, '\n');

		cout << "Podaj rocznik: ";
		cin >> cars[i].year;
		cin.ignore(1000, '\n');
	}

	for (int i = 0; i < carsQuant; i++) {
		cout << "Marka: " << cars[i].brand << ", Rocznik: " << cars[i].year << endl;
	}

	system("PAUSE");
	return 0;
}