/*sum of mothly sales*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <array>

using std::cout;
using std::cin;
using std::endl;

const int YEARS = 3;
const int MONTHS = 12;

int main() {

	const char * years[MONTHS] = {
		"ROK 1",
		"ROK 2",
		"ROK 3"
	};

	int sells[YEARS][MONTHS]{
		{ 13, 31, 51, 25, 512, 5, 12, 5, 1, 2, 1, 5 },
		{ 41, 412, 51, 21, 5, 21, 51, 25, 51, 23, 13, 1 },
		{ 1, 4, 15, 124, 12, 241, 12, 41, 25, 12, 41 ,31 }
	};

	int sum = 0;

	for (int y = 0; y < YEARS; y++) {
		cout << years[y] << ":\t";

		for (int m = 0; m < MONTHS; m++) {
			cout << sells[y][m] << "\t";

			sum += sells[y][m];
		}
		cout << endl;
		cout << "Suma w " << years[y] << ": " << sum << endl;
	}


	system("PAUSE");
	return 0;
