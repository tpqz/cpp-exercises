/*function&arrays2*/

#include "stdafx.h"
#include <iostream>
#include <cstdlib>

const int SIZE = 10;
using namespace std;

double fillArray(double arr[], int size);
void showArray(double arr[], int size);
void reverseArray(double arr[], int size);

int main() {

	double tablica[SIZE];

	fillArray(tablica, SIZE);
	showArray(tablica, SIZE);
	reverseArray(tablica, SIZE);
	showArray(tablica, SIZE);

	system("PAUSE");
	return 0;
}

double fillArray(double arr[], int size) {
	double counter = 0;
	int i = 0;

	cout << "Podaj liczbe: ";
	while (cin >> arr[i] && i != size - 1) {
		counter++;
		i++;
		cout << "Podaj kolejna liczbe: ";
	}
	return counter;
}

void showArray(double arr[], int size) {
	for (int i = 0; i < size; i++) {
		if (arr[i] >= 0) {
			cout << "Wartosc numer " << i + 1 << " = " << arr[i] << endl;
		}
		else {
			continue;
		}
	}
}

void reverseArray(double arr[], int size) {
	double temp = 0;

	for (int i = 0, j = size - 1; i < size / 2; i++, j--) {
		temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}
