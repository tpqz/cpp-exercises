#include "stdafx.h"
#include <iostream>

int main() {
	double km, litersStart, litersNow, gasoline, combustion;

	std::cout << "Podaj przejechane kilometry: ";
	std::cin >> km;

	std::cout << "Podaj ile litrow benzyny bylo na poczatku podrozy: ";
	std::cin >> litersStart;

	std::cout << "Podaj ile litrow benzyny jest teraz: ";
	std::cin >> litersNow;

	gasoline = litersStart - litersNow;
	combustion = (gasoline * 100) / km;

	std::cout << "Samochod osi�ga spalanie: " << combustion << " l/100km \n";

	std::cin.get();
	std::cin.get();
	return 0;
}