/*switch*/

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

int main() {

	char ch;
	cout << "r) Roslinozerca\tp) Pianista\nd) Drzewo\tg) Gra\n"
		"Podaj dowolna cyfre aby zakonczyc dzialanie\n"
		"Prosze podac litere r,p,t lub g: ";
	cin >> ch;
	cout << endl;

	while (!isdigit(ch)) {

		switch (ch) {

		case 'r':
		case 'R': cout << "Slimak jest roslinozerca\n\n";
			break;

		case 'p':
		case 'P': cout << "Chopin jest pianista\n\n";
			break;

		case 'd':
		case 'D': cout << "Klon jest drzewem\n\n";
			break;

		case 'g':
		case 'G': cout << "CS jest gra\n\n";
			break;

		default: cout << "Zly wybor\n\n";
			break;
		}
		cout << "r) Roslinozerca\tp) Pianista\nd) Drzewo\tg) Gra\n"
			"Prosze podac litere r,p,t lub g: ";

		cin >> ch;
		cout << endl;

	}

	system("PAUSE");
	return 0;


