/*
 * main.cpp
 *
 *  Created on: Aug 26, 2016
 *      Author: tqpz
 */

#include "move.h";

int main(){

	Move test(4,7);
	Move plus(51,51);

	test.showmove();
	test.add(plus);

	test.showmove();
	plus.showmove();

	test.reset();
	test.showmove();

return 0;
}


