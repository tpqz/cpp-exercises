/*
 * move.cpp
 *
 *  Created on: Aug 26, 2016
 *      Author: tqpz
 */

#include "move.h"
#include <iostream>

Move::Move(double a, double b){
	x = a;
	y = b;
}

void Move::showmove() const{
	std::cout << "X value: " << x << ", Y value: " << y << std::endl;
}

Move Move::add(Move & m){

	x += m.x;
	y += m.y;

	return *this;
}

void Move::reset(double a, double b){
	x = a;
	y = b;
}
