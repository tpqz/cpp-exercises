/*cpunting chars in file*/

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;
const int SIZE = 50;

int main() {

	int counter = 0;
	char value;
	char filename[SIZE];
	ifstream inFile;

	cout << "Podaj nazwe pliku z danymi: ";
	cin.getline(filename, SIZE);

	inFile.open(filename);

	if (!inFile.is_open()) {
		cout << "Otwarcie pliku " << filename << " nie powiodlo sie\n";
		cout << "Program zostanie zakonczony\n";
		system("PAUSE");
		exit(EXIT_FAILURE);
	}

	inFile.get(value);

	while (inFile.good()) {
		counter++;
		cout << value;
		inFile.get(value);
	}

	if (inFile.eof()) {
		cout << "\nKoniec pliku.\n";
	}
	else if (inFile.fail()) {
		cout << "Blad wczytywania.\n";
	}
	else {
		cout << "Nieznany blad.\n";
	}


	if (counter == 0) {
		cout << "Nie przetworzono zadnych znakow.\n";
	}
	else {
		cout << "Wczytanych znakow: " << counter << endl;
	}

	inFile.close();

	system("PAUSE");
	return 0;
}