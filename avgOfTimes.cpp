/*avg of times*/

#include "stdafx.h"
#include <iostream>
#include <array>
#include <iomanip>


using namespace std;

int main() {

	array<double, 3> sprintTime;
	int i;

	for (i = 0; i <= 2; i++) {
		cout << "Podaj czas nr " << i + 1 << ": ";
		cin >> sprintTime[i];
	}

	for (i = 0; i <= 2; i++) {
		cout << "Czas nr: " << i + 1 << "= " << sprintTime[i] << endl;
	}

	double avg = (sprintTime[0] + sprintTime[1] + sprintTime[2]) / 3;
	cout << setprecision(4) << "Srednia wynosi: " << (avg) << endl;

	system("PAUSE");
	return 0;
}