/*chapter 7 exercises*/

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;
const int SIZE = 50;

int main() {

	void igor();
	int tofu(int x);
	double mpg(double x, double y);

	system("PAUSE");
	return 0;
}

void igor() {

}

int tofu(int x) {
	return (float)x;
}

double mpg(double x, double y) {
	return x, y;
}

long summation(long arr[], int x) {
	return (double)x;
}

double doctor(const string x) {

}

void third(int arr[], int size, int x) {
	for (int i = 0; i < size; i++) {
		arr[i] = x;
	}
}

void fourth(int * begin, int * end, int value) {
	for (int * pt = begin; pt != end; pt++) {
		*pt = value;
	}
}

double five(const double arr[], int size) {
	double max = arr[0];
	for (int i = 0; i < size; i++) {
		if (arr[i] >= max) {
			max = arr[i];
		}
	}
	return max;
}

int replace(char * str, char c1, char c2) {
	int counter = 0;

	while (*str) {
		if (*str == c1) {
			c1 = c2;
			counter++;
		}
		str++;
	}
	return counter;
}

struct applicant {
	char name[30];
	int credit_ratings[3];
};

void str_func(applicant x) {
	cout << x.name;
	for (int i = 0; i < sizeof(x.credit_ratings); i++) {
		cout << x.credit_ratings;
	}
}

void display(const applicant * pa) {

	cout << pa->name << endl;
	for (int i = 0; i < 3; i++) {
		cout << pa->credit_ratings[i] << endl;
	}
}

void f1(applicant * a);
const char * f2(const applicant * a1, const applicant * a2);

typedef void(*p1_f1)(applicant*);
p1_f1 p1 = f1;


typedef const char * (*p2_f2)(const applicant *, const applicant *);
p2_f2 p2 = f2;

p1_f1 ap[5];
p2_f2(*pa)[10];