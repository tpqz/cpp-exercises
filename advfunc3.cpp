/*
 * advfunc.cpp
 *
 *  Created on: Aug 19, 2016
 *      Author: tqpz
 */

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

void toUpperCase(string & s);

int main() {

	string  empty = "";
	cout << "Type exit to quit" << endl;
	toUpperCase(empty);

return 0;
}

void toUpperCase(string & s){

	cout << "Podaj slowa: ";
	cin >> s;
	std::transform(s.begin(), s.end(), s.begin(), ::toupper);
	cout << s << endl;;

	string key = "EXIT";

	while (s != key){

		toUpperCase(s);

	}

}
