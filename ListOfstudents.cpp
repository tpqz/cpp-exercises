/*list of students*/

#include "stdafx.h"
#include <iostream>
#include <cstdlib>

using namespace std;

const int SLEN = 30;

struct student {
	char fullName[SLEN];
	char hobby[SLEN];
	int ooplevel;
};

int getinfo(student pa[], int n);
void display1(student st);
void display2(const student * pa);
void display3(const student pa[], int n);

int main() {

	int classSize;
	cout << "Podaj wielkosc grupy: ";
	cin >> classSize;

	while (cin.get() != '\n')
		continue;

	student * ptr_stu = new student[classSize];
	int entered = getinfo(ptr_stu, classSize);
	for (int i = 0; i < entered; i++) {
		display1(ptr_stu[i]);
		display2(&ptr_stu[i]);
	}

	display3(ptr_stu, entered);
	delete[] ptr_stu;
	cout << "Gotowe\n";

	system("PAUSE");
	return 0;
}

int getinfo(student pa[], int n) {
	int counter = 0;

	for (int i = 0; i < n; i++) {
		cout << "Podaj nazwisko: ";
		cin >> pa[i].fullName;

		cout << "Podaj hobby: ";
		cin >> pa[i].hobby;

		cout << "Podaj level: ";
		cin >> pa[i].ooplevel;

		counter += 1;
	}

	return counter;
}

void display1(student st) {
	cout << "DISPLAY1:\n";
	cout << st.fullName << endl;
	cout << st.hobby << endl;
	cout << st.ooplevel << endl;
	cout << endl;
}

void display2(const student * pa) {
	cout << "DISPLAY2:\n";
	cout << pa->fullName << endl;
	cout << pa->hobby << endl;
	cout << pa->ooplevel << endl;
	cout << endl;
}

void display3(const student pa[], int n) {
	cout << "DISPLAY3:\n";
	for (int i = 0; i < n; i++) {
		cout << pa[i].fullName << endl;
		cout << pa[i].hobby << endl;
		cout << pa[i].ooplevel << endl;
		cout << endl;
	}
}