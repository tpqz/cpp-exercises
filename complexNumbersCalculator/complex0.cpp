/*
 * complex0.cpp
 *
 *  Created on: Sep 1, 2016
 *      Author: tqpz
 */

#include "complex0.h"

	Complex::Complex(){
		real = imaginary = 0;
	};

	Complex::Complex(double r, double i){
		real = r;
		imaginary = i;
	};

	Complex::~Complex(){};

	Complex Complex::operator+(const Complex & c) const {
		return Complex(real + c.real, imaginary + c.imaginary);
	};

	Complex Complex::operator-(const Complex & c) const {
		return Complex(real - c.real, imaginary - c.imaginary);
	};

	Complex Complex::operator*(const Complex & c) const {
		return Complex(((real * c.real)-(imaginary * c.imaginary)),((real * c.imaginary) + (imaginary * c.real)));
	};

	Complex Complex::operator~() const {
		return Complex(real, -imaginary);
	}

	Complex operator*(double x, const Complex & c) {
		return Complex(x * c.real, x * c.imaginary);
	};

	ostream & operator<<(ostream & os, const Complex c){
		os << "(" << c.real << ", " << c.imaginary << "i)\n";
		return os;
	};

	istream & operator>>(istream & is, Complex & c){
		cout << "Skladowa rzeczywista: ";
		is >> c.real;
		cout << "Skladowa urojona: ";
		is>> c.imaginary;
		cout << endl;
		return is;
	};
