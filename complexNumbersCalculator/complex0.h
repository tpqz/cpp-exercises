/*
 * complex0.h
 *
 *  Created on: Aug 30, 2016
 *      Author: tqpz
 */

#ifndef COMPLEX0_H_
#define COMPLEX0_H_

#include <iostream>
using namespace std;

class Complex{

private:
	double real;
	double imaginary;

public:
	Complex();
	Complex(double r, double i);
	~Complex();
	Complex operator+(const Complex & c) const;
	Complex operator-(const Complex & c) const;
	Complex operator*(const Complex & c) const;
	Complex operator~() const;
	friend Complex operator*(double x, const Complex & c);
	friend ostream & operator<<(ostream & os, const Complex c);
	friend istream & operator>>(istream & os, Complex & c);
};

#endif /* COMPLEX0_H_ */
