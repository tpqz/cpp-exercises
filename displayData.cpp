/*displaying input data*/

#include "stdafx.h"
#include <iostream>
#include <array>
#include <cstring>
#include <vector>

const int SIZE = 10;
using namespace std;

int main() {
	char name[64], surname[64];
	int grade, age;

	cout << "Jak masz na imie? : ";
	cin.getline(name, 64);

	cout << "Jak masz na nazwisko? : ";
	cin.getline(surname, 64);

	cout << "Na jaka zaslugujesz ocene? : ";
	cin >> grade;

	cout << "Ile masz lat? : ";
	cin >> age;

	cout << "Nazwisko: " << surname << ", " << name << endl;
	cout << "Ocena: " << grade - 1 << endl;
	cout << "Wiek: " << age << endl;

	system("PAUSE");

	return 0;
}